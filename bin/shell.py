import mysql.connector
from datetime import date

# this program functions with a shell-like interface. this is that shell
class Shell:
    def __init__(self, database):
        self.__database = database # the database we're working with


    # main command runner. runs functions based on specified input
    def __runCommand(self, cmd):
        availableCommands = {"add": self.__addRecord}

        try:
            availableCommands[cmd]()
        except:
            print("Command \"%s\" not found.", cmd)


    # generic flow for adding an income or an expense
    def __addRecord(self):
        availableCommands = {"i": self.__addIncome, "e": self.__addExpense}
        recordType = None
        while(recordType == None):
            response = input("Add [I]ncome or [E]xpense? ")
            recordType = response.lower()[0]
            if recordType not in "ie":
                recordType = None

        recordDesc = input("Record description: ")
        recordDate = None
        while(recordDate == None):
            response = input("Record date[YYYY-MM-DD] (leave blank for today): ")
            if response == "":
                recordDate = date.today()
            else:
                fields = response.split("-")
                if len(fields) != 3:
                    print("Date must be formatted as [YYYY-MM-DD].")
                    continue

                try:
                    recordDate = date(year=int(fields[0]), month=int(fields[1]), day=int(fields[2]))
                except:
                    print("Date must be formatted as [YYYY-MM-DD].")
                    continue

        recordAmount = None
        while(recordAmount == None):
            response = input("Record amount: ")
            try:
                recordAmount = round(float(response), 2)
            except:
                print("Floating point decimal numbers only.")
                continue

        availableCommands[recordType](recordDesc, recordDate, recordAmount)
        self.__database.commit()

    # flow to add income record
    def __addIncome(self, desc, incomeDate, amount):
        cursor = self.__database.cursor()
        sql = "INSERT INTO Incomes (IncomeDesc, IncomeDate, IncomeAmount) VALUES (%s, %s, %f)"
        cursor.execute(sql, (desc, incomeDate, amount))


    # flow to add expense record
    def __addExpense(self, desc, expenseDate, amount):
        cursor = self.__database.cursor()
        sql = "INSERT INTO Expenses (ExpenseDesc, ExpenseDate, ExpenseAmount) VALUES (%s, %s, %f)"
        cursor.execute(sql, (desc, expenseDate, amount))

    # selects income based on month and/or year
    def __pullIncome(self, month=None, year=None):
        if month == None:
            monthString = ""
        else:
            monthString = "MONTH(IncomeDate) = %d".format(month)

        if year == None:
            yearString = ""
        else:
            yearString = "YEAR(IncomeDate) = %d".format(year)

        cursor = self.__database.cursor()
        sql = "SELECT IncomeDate, IncomeAmount, IncomeDesc FROM Incomes"
        whereString = ""

        if monthString == "" and yearString == "":
            whereString = ""
        elif monthString != "" and yearString != "":
            whereString = monthString + " AND " + yearString
        elif monthString != "":
            whereString = monthString
        elif yearString != "":
            whereString = yearString
        else:
            whereString = "error in shell.py > Shell.__viewIncome()."


        if whereString != "":
            sql += " WHERE " + whereString

        cursor.execute(sql)

        return cursor.fetchall()

    # selects expense based on month and/or year
    def __pullExpense(self, month=None, year=None):
        if month == None:
            monthString = ""
        else:
            monthString = "MONTH(ExpenseDate) = %d".format(month)

        if year == None:
            yearString = ""
        else:
            yearString = "YEAR(ExpenseDate) = %d".format(year)

        cursor = self.__database.cursor()
        sql = "SELECT ExpenseDate, ExpenseAmount, ExpenseDesc FROM Expenses"
        whereString = ""

        if monthString == "" and yearString == "":
            whereString = ""
        elif monthString != "" and yearString != "":
            whereString = monthString + " AND " + yearString
        elif monthString != "":
            whereString = monthString
        elif yearString != "":
            whereString = yearString
        else:
            whereString = "error in shell.py > Shell.__viewExpense()."


        if whereString != "":
            sql += " WHERE " + whereString

        cursor.execute(sql)

        return cursor.fetchall()
