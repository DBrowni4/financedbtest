import mysql.connector
import yaml

# loads the hostname, username, database, and password
authInfo = None
with open("config/auth.yaml", 'r') as authConfig:
    authInfo = yaml.safe_load(authConfig)


# test loading
def test_auth():
    assert(authInfo != None)


# initialize connection to FinanceDB
if __name__ == "__main__":
    FinanceDB = mysql.connector.connect(
        host=authInfo["host"],
        user=authInfo["username"],
        password=authInfo["password"],
        database=authInfo["database"]
    )

